module Homework

implicit none
contains


    subroutine FindMaxCoordinates(A, x1, y1, x2, y2)
        implicit none
        include "mpif.h"
        real(8), intent(in), dimension(:,:) :: A
        integer(4), intent(out) :: x1, y1, x2, y2
        integer(4) x1_proc, y1_proc, x2_proc, y2_proc
        integer(4) :: n, L, R, Up, Down, m, tmp
        real(8), allocatable :: current_column(:), B(:,:)
        real(8) :: current_sum, max_sum, total_max_sum
        logical :: transpos
        integer mpisize, mpierr, mpirank, i, j
        real(8), allocatable, dimension(:) :: all_max_sum   
        integer(4), allocatable, dimension(:):: X_1, X_2, Y_1, Y_2

        call mpi_comm_size(MPI_COMM_WORLD, mpisize, mpierr)
        call mpi_comm_rank(MPI_COMM_WORLD, mpirank, mpierr)
      

        m = size(A, dim=1) 
        n = size(A, dim=2) 
        transpos = .FALSE.



        if (m < n) then 
            transpos = .TRUE.   
            B = transpose(A)
            m = size(B, dim=1) 
            n = size(B, dim=2) 
        else
            B = A     
        endif


        allocate(current_column(m)) 
        
        max_sum=B(1,1)
        x1=1
        y1=1
        x2=1
        y2=1

        x1_proc=1
        y1_proc=1
        x2_proc=1
        y2_proc=1
       

        do L=mpirank+1, n, mpisize
 
            current_column = B(:, L)
                                                                   
            do R=L,n
 
                if (R > L) then 
                    current_column = current_column + B(:, R)
                endif
                

                call FindMaxInArray(current_column, current_sum, Up, Down) 

                      
                if (current_sum > max_sum) then

                    max_sum = current_sum
                    x1_proc = Up
                    x2_proc = Down
                    y1_proc = L
                    y2_proc = R
                endif
            end do
        end do



        deallocate(current_column)

        if (mpirank.eq.0) then
            allocate(all_max_sum(mpisize)) 
            allocate(X_1(mpisize))     
            allocate(X_2(mpisize)) 
            allocate(Y_1(mpisize)) 
            allocate(Y_2(mpisize)) 
        endif


        call mpi_gather(max_sum, 1, MPI_REAL8, all_max_sum, 1, MPI_REAL8, 0, MPI_COMM_WORLD, mpierr)
        call mpi_gather(x1_proc, 1, MPI_INTEGER4, X_1, 1, MPI_INTEGER4, 0, MPI_COMM_WORLD, mpierr)
        call mpi_gather(x2_proc, 1, MPI_INTEGER4, X_2, 1, MPI_INTEGER4, 0, MPI_COMM_WORLD, mpierr)
        call mpi_gather(y1_proc, 1, MPI_INTEGER4, Y_1, 1, MPI_INTEGER4, 0, MPI_COMM_WORLD, mpierr)
        call mpi_gather(y2_proc, 1, MPI_INTEGER4, Y_2, 1, MPI_INTEGER4, 0, MPI_COMM_WORLD, mpierr)


        if (mpirank.eq.0) then

            total_max_sum=all_max_sum(1)
            j=1
  
            if (mpisize>1) then
                do i=2,mpisize
                    if (all_max_sum(i)>total_max_sum) then
                        total_max_sum=all_max_sum(i)
                        j=i
                    endif
                enddo
            endif

            x1=X_1(j)
            x2=X_2(j)
            y1=Y_1(j)
            y2=Y_2(j)

            if (transpos) then  
                tmp = x1
                x1 = y1
                y1 = tmp
    
                tmp = y2
                y2 = x2
                x2 = tmp
            endif

        endif


        call mpi_bcast(x1, 1, MPI_INTEGER4, 0, MPI_COMM_WORLD, mpierr)
        call mpi_bcast(x2, 1, MPI_INTEGER4, 0, MPI_COMM_WORLD, mpierr)
        call mpi_bcast(y1, 1, MPI_INTEGER4, 0, MPI_COMM_WORLD, mpierr)
        call mpi_bcast(y2, 1, MPI_INTEGER4, 0, MPI_COMM_WORLD, mpierr)

        if (mpirank.eq.0) then
            deallocate(all_max_sum) 
            deallocate(X_1)     
            deallocate(X_2) 
            deallocate(Y_1) 
            deallocate(Y_2) 
        endif


    end subroutine


    subroutine FindMaxInArray(a, Sum, Up, Down)
        real(8), intent(in), dimension(:) :: a
        integer(4), intent(out) :: Up, Down
        real(8), intent(out) :: Sum
        real(8) :: cur_sum
        integer(4) :: minus_pos, i

        Sum = a(1)
        Up = 1
        Down = 1
        cur_sum = 0
        minus_pos = 0



        do i=1, size(a)
            cur_sum = cur_sum + a(i)
            if (cur_sum > Sum) then
                Sum = cur_sum
                Up = minus_pos + 1
                Down = i
            endif
         
            if (cur_sum < 0) then
                cur_sum = 0
                minus_pos = i
            endif

        enddo

    end subroutine FindMaxInArray


end module Homework



